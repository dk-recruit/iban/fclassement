package fr.iban.fclassement.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.massivecraft.factions.Faction;

import fr.darkonia.core.Core;
import fr.darkonia.core.sql.Callback;
import fr.darkonia.core.sql.DbManager;
import fr.darkonia.core.sql.runnables.QueryRunnable;
import fr.darkonia.core.sql.runnables.UpdateRunnable;
import fr.iban.fclassement.FClassement;

public class SqlCalls {

	private static final String CREATE_TABLE_SQL="CREATE TABLE IF NOT EXISTS Factions ("
			+ "id INT AUTO_INCREMENT PRIMARY KEY,"
			+ "fid VARCHAR(36) NOT NULL,"
			+ "points INT NOT NULL,"
			+ "UNIQUE KEY (fid))";


	//ADD FACTION TO DB
	public static void updateFactionToDatabase(String fid, int points, Callback<Integer, SQLException> callback) {
		try {
			new UpdateRunnable(DbManager.BD.getDbAccess().getConnection(), "INSERT INTO `Factions` (fid, points) VALUES ('"+fid+"', "+points+") ON DUPLICATE KEY UPDATE points=VALUES(points)", callback).runTaskAsynchronously(Core.getInstance());
		} catch (IllegalArgumentException | IllegalStateException | SQLException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//SELECT ALL FACTIONS
	public static void selectAllFactions(Callback<ResultSet, SQLException> callback) {
		try {
			new QueryRunnable(DbManager.BD.getDbAccess().getConnection(), "SELECT * FROM `Factions`", callback).runTaskAsynchronously(Core.getInstance());
		} catch (IllegalArgumentException | IllegalStateException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//CREATE TABLES
	public static void createTable(Callback<Integer, SQLException> callback) {
		try {
			new UpdateRunnable(DbManager.BD.getDbAccess().getConnection(), CREATE_TABLE_SQL , callback).runTaskAsynchronously(Core.getInstance());
		} catch (IllegalArgumentException | IllegalStateException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//REMOVE FACTION
	public static void removeFactionFromDatabase(String fid, Callback<Integer, SQLException> callback) {
		try {
			new UpdateRunnable(DbManager.BD.getDbAccess().getConnection(), "DELETE from `Factions` WHERE fid='"+fid+"'", callback).runTaskAsynchronously(Core.getInstance());
		} catch (IllegalArgumentException | IllegalStateException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void createTablesifNotExists() {
		createTable((rs, t) -> {
			if(rs == null) {
				System.out.println("Classement : Erreur lors de la création de la table Factions(fid,points).");
				t.printStackTrace();
			}
		});
	}

	public static void cacheFromDb() {
		selectAllFactions((rs, t) -> {
			if(t == null) {
				try {
					while(rs.next()) {
						String fid = rs.getString("fid");
						Integer points = rs.getInt("points");
						FClassement.getInstance().getFactionsData().put(fid, points);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else {
				t.printStackTrace();
			}
		});
	}

	public static void saveFactionToDb(String fid, int points) {
		updateFactionToDatabase(fid, points, (result, thrown) -> {
			if(thrown != null) thrown.printStackTrace();
		});
	}

	public static void saveAllFactionsToDb() {
		FClassement.getInstance().getFactionsData().forEach((fid, points) -> {
			SqlCalls.saveFactionToDb(fid, points);
		});
	}

	public static void removeFaction(Faction f) {
		removeFactionFromDatabase(f.getId(), (rs, t) -> {});
		FClassement.getInstance().getFactionsData().remove(f.getId());
	}
}