package fr.iban.fclassement;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.bukkit.entity.Player;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;

public class FClassementMethods {

	private FClassement instance;
	private Map<String, Integer> factionsData = FClassement.getInstance().getFactionsData();

	public FClassementMethods(FClassement instance) {
		this.instance = instance;
	}


	/**
	 * @param f = faction
	 * @return Les points de la faction.
	 */
	public int getPoints(Faction f) {
		if(!f.isWilderness()) {
			if(factionsData.containsKey(f.getId())) {
				return factionsData.get(f.getId());
			}
		}
		return 0;
	}

	/**
	 * @param p = joueur
	 * @return Les points de la faction du joueur.
	 */
	public int getPoints(Player p) {
		FPlayer fp = FPlayers.getInstance().getByPlayer(p);
		Faction f = fp.getFaction();
		return getPoints(f);
	}
	
	public void setPoints(Faction f, int points) {
		if(!f.isWilderness()) {
			factionsData.put(f.getId(), points);
		}
	}
	
	public void addPoints(Faction f, int points) {
		setPoints(f, getPoints(f) + points);
	}
	
	public void removePoints(Faction f, int points) {
		if(getPoints(f) >= points)
		setPoints(f, getPoints(f) - points);
		else setPoints(f, 0);
	}
	
	public void setPoints(Player p, int points) {
		FPlayer fp = FPlayers.getInstance().getByPlayer(p);
		Faction f = fp.getFaction();
		setPoints(f, points);
	}
	
	public void addPoints(Player p, int points) {
		FPlayer fp = FPlayers.getInstance().getByPlayer(p);
		Faction f = fp.getFaction();
		addPoints(f, points);
	}
	
	public void removePoints(Player p, int points) {
		FPlayer fp = FPlayers.getInstance().getByPlayer(p);
		Faction f = fp.getFaction();
		removePoints(f, points);
	}
	
	

	/**
	 * @param f = faction
	 * @return La position de la faction
	 */
	public int getRank(Faction f) {
		if(!f.isWilderness()) {
			int i = 0;
			for(String fid : sortedMap().keySet()) {
				i++;
				if(fid.equalsIgnoreCase(f.getId())) {
					return i;
				}
			}
		}
		return -1;
	}
	
	/**
	 * @param p = joueur
	 * @return La position de la faction du joueur
	 */
	public int getRank(Player p) {
		FPlayer fp = FPlayers.getInstance().getByPlayer(p);
		Faction f = fp.getFaction();
		return getRank(f);
	}

	/**
	 * @return Top 10 des factions par points.
	 */
	public Map<String, Integer> getTop10(){
        LinkedHashMap<String, Integer> sortedMap = new LinkedHashMap<>();
        factionsData.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(10)
                .forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));
		return sortedMap;
	}

	/**
	 * @return Toutes les factions classées par points
	 */
	public Map<String, Integer> sortedMap(){
        LinkedHashMap<String, Integer> sortedMap = new LinkedHashMap<>();
        factionsData.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));
		return sortedMap;
	}
	
	/**
	 * @return Liste des messages à afficher dans le classement
	 */
	public List<String> getClassementStrings(Player p){
		FPlayer fp = FPlayers.getInstance().getByPlayer(p);
		Faction f = fp.getFaction();
		List<String> stringList = instance.getConfig().getStringList("showing");
		List<String> top10StringList = new ArrayList<String>();
		
		
		int rank = 0;
		for(Entry<String, Integer> entry : getTop10().entrySet()) {
			rank++;
			Faction faction = Factions.getInstance().getFactionById(entry.getKey());
			top10StringList.add(instance.getConfig().getString("top")
					.replaceAll("%Position%", rank+"")
					.replaceAll("%Faction%", faction.getTag())
					.replaceAll("%Points%", entry.getValue()+""));
		}
		
		for(int i = 0 ; i < stringList.size() ; i++) {
			if(stringList.get(i).equalsIgnoreCase("(top)")) {
				stringList.remove(i);
				
				for(int index = top10StringList.size()-1 ; index >= 0 ; index--) {
					stringList.add(i, top10StringList.get(index));
				}
				
			}
			
			if(stringList.get(i).contains("%yposition%")) {
				
				String string = stringList.get(i).replaceAll("%yposition%", getRank(f)+"")
						.replaceAll("%yfaction%", f.getTag())
						.replaceAll("%ypoints%", getPoints(f)+"");
				stringList.remove(i);
				stringList.add(i, string);
				
			}
			
		}
		stringList.replaceAll(s -> s.replaceAll("&", "§"));
		return stringList;
	}
	
	

}
