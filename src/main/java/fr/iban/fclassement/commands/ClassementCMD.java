package fr.iban.fclassement.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;

import fr.iban.fclassement.FClassement;
import fr.iban.fclassement.sql.SqlCalls;

public class ClassementCMD implements CommandExecutor {

	private FClassement instance;

	public ClassementCMD(FClassement instance) {
		this.instance = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		if(sender instanceof Player) {

			Player p = (Player)sender;

			switch (args.length) {
			case 0:
				String[] stringArray = instance.getMethods().getClassementStrings(p).stream().toArray(String[]::new);
				p.sendMessage(stringArray);
				if(stringArray.length == 0) {
					p.sendMessage("array vide");
				}
				break;
			case 1:
				if(args[0].equalsIgnoreCase("help")) {
					p.sendMessage(new String[] {
							"§7/classement - affiche le classement.",
							"§7/classement resetall - reset tout le classement.",
							"§7/classement set <faction> <points>  - définir les points pour une faction.",
							"§7/classement remove <faction> <points>  - supprimer des points à une faction.",
							"§7/classement reset <faction>  - remettre les points d'une faction à zéro."
					});
					return true;
				}else if(args[0].equalsIgnoreCase("resetall")) {
					instance.getFactionsData().forEach((fid, points) -> {
						Faction faction = Factions.getInstance().getFactionById(fid);
						SqlCalls.removeFaction(faction);
					});
					return true;
				}else p.chat("/classement help");
				break;
			case 2:
				if(args[0].equalsIgnoreCase("reset")) {
					Faction f = Factions.getInstance().getByTag(args[1]);
					if(f != null)
						instance.getMethods().setPoints(f, 0);
					return true;
				}else p.chat("/classement help");
				break;
			case 3:
				if(args[0].equalsIgnoreCase("add")) {

					if(args.length == 3) {

						Faction f = Factions.getInstance().getByTag(args[1]);
						if(f != null) {

							instance.getMethods().addPoints(f, parseInt(args[2]));
							p.sendMessage("§aVous avez ajouté " + parseInt(args[2]) + " points à la faction" + f.getTag());

						} else p.sendMessage("La faction " + args[1] + " n'a pas été trouvée.");

					}else p.sendMessage("USAGE : /classement add <faction> <points  - ajouter des points à une faction. ");

				}else if(args[0].equalsIgnoreCase("set")) {

					if(args.length == 3) {

						Faction f = Factions.getInstance().getByTag(args[1]);
						
						if(f != null) {

							instance.getMethods().setPoints(f, parseInt(args[2]));
							p.sendMessage("§aVous avez défini les points de la faction " + f.getTag() + " à " + parseInt(args[2]) + ".");

						} else p.sendMessage("La faction " + args[1] + " n'a pas été trouvée.");

					}else p.sendMessage("USAGE : /classement set <faction> <points  - définir les points d'une faction. ");					


				}else if(args[0].equalsIgnoreCase("remove")) {

					if(args.length == 3) {

						Faction f = Factions.getInstance().getByTag(args[1]);
						
						if(f != null) {

							instance.getMethods().removePoints(f, parseInt(args[2]));
							p.sendMessage("§aVous avez retiré " + parseInt(args[2]) + " points à la faction" + f.getTag());

						} else p.sendMessage("La faction " + args[1] + " n'a pas été trouvée.");

					}else p.sendMessage("USAGE : /classement remove <faction> <points  - retirer des points à une faction. ");					

				}else p.chat("/classement help");
				
				break;
			default:
				break;
			}
		}
		return false;
	}

	private int parseInt(String string) {
		try {
			int points = Integer.parseInt(string);
			return points;
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return 0;
		}
	}

}
