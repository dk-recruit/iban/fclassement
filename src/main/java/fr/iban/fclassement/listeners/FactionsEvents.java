package fr.iban.fclassement.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.massivecraft.factions.Faction;
import com.massivecraft.factions.event.FactionCreateEvent;
import com.massivecraft.factions.event.FactionDisbandEvent;

import fr.iban.fclassement.FClassement;
import fr.iban.fclassement.sql.SqlCalls;

public class FactionsEvents implements Listener {
	
	@EventHandler
	public void onDisband(FactionDisbandEvent e) {
		SqlCalls.removeFaction(e.getFaction());
	}
	
	@EventHandler
	public void onCreate(FactionCreateEvent e) {
		Faction f = e.getFaction();
		SqlCalls.saveFactionToDb(f.getId(), 0);
		FClassement.getInstance().getFactionsData().put(f.getId(), 0);
	}

}
