package fr.iban.fclassement;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.iban.fclassement.commands.ClassementCMD;
import fr.iban.fclassement.listeners.FactionsEvents;
import fr.iban.fclassement.sql.SqlCalls;

public class FClassement extends JavaPlugin {
	
	private Map<String, Integer> factionsData;
	private static FClassement instance;
	private FClassementMethods classement;
	
	@Override
	public void onEnable() {
		instance = this;
		factionsData = new HashMap<String, Integer>();
		classement = new FClassementMethods(this);
		saveDefaultConfig();
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new FactionsEvents(), this);
		getCommand("classement").setExecutor(new ClassementCMD(this));
		SqlCalls.createTablesifNotExists();
		SqlCalls.cacheFromDb();
		Bukkit.getScheduler().runTaskTimer(this, () -> {
			SqlCalls.saveAllFactionsToDb();
		}, 10*20*60, 10*20*60);
	}
	
	@Override
	public void onDisable() {
		SqlCalls.saveAllFactionsToDb();
	}
	
	@Override
	public void onLoad() {
		
	}

	public static FClassement getInstance() {
		return instance;
	}

	public Map<String, Integer> getFactionsData() {
		return factionsData;
	}

	public FClassementMethods getMethods() {
		return classement;
	}

}
